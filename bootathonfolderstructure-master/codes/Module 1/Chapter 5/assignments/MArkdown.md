> ### Post-Test :
<pre>
Q.1. Refer the data sheet of any instrumentation amplifier IC and list down the important specifications for the same
 

Q.2. Which of the following are the application areas of instrumentation amplifiers?

1.Medical instrumentation-EEG monitors, blood pressure monitors, defibrillators.
<b>2.Audio applications - microphone preamplifiers, Video applications.</b>
3.Signal conditioning and power control applications-monitor & control motor torque & speed.
4.All of the above.<


Q.3. Define the following parameters:
1. CMRR
2. Input bias and offset currents and offset voltages.
3. AC common mode rejection.
4. Gain

 

Q.4.Obtain the exact expression for (Vout/Vin) of the circuit shown.

<b>1.Vout = - [Ra + Rb + (RaRb/Rc)]Vin/Ra</b>
2.Vout = - [Rb + Rc + (RbRc/Rd)]Vin/Ra
3.Vout = - [Rb + (RbRc/Rd)]Vin/Ra
4.Vout = - [Ra + Rb + (RaRb/Rd)]Vin/Rd


Q.5. For an instrumentation amplifier shown here, verify that:
Vo = [1 + (R2/R1) + (2R2/R)](V2 - V1)
![Picture6](F:\ICA\10th jan\ICA lab\labs\exp4\Picture6.jpg "From Simulator")
</pre>
 

 > ## Pre-Test :

 <pre>

Q.1. Instrumentation amplifier is a specialized difference amplifier.



The Correct Answer Is B

Q.2. Instrumentation amplifier is an example of _____________op-amp application to measure _____________output from transducers.

1.linear, high voltage-low frequency
<b>2.linear, low voltage-low frequency </b>
3.non-linear, high voltage-low frequency
4.non-linear, low voltage-high frequency


Q.3. The important features of an instrumentation amplifier are ______________

1.high gain accuracy, high CMRR, low dc offset.
2.high input impedance at both input terminals, low output impedance.
3.high gain stability with low temperature coefficient
<b>4.All of these.</b>


Q.4. Instrumentation amplifier can be designed using _______Op-amps.

<b>1.1</b>
2.2
3.3
4.2 or 3


Q.5. In applications such as high pressure boiler systems, toxic chemical processes, the system is interfaced to instrumentation amplifier through long cables. This results in noise pick up and also interference due to ground loop.
Which of the statements is true?

1.Use pair of wires for noise cancellation.
2.Use shielded wires to reduce noise.
3.Shielded wire pairs contribute to distributed capacitance and degrade CMRR.
4.All of the above.
 </pre>

 > ## Procedure :

<pre>
1.From the dropdown list select any desired component and place it on the canvas.
2.Repeat the above step till all the required components are placed on the canvas.
3.Select the type of input from the dropdown list provided.
4.Now click on the ‘Connect’ button and then click on the bubbles of the respective component to establish a connection between them.
5.‘Undo’ button is to undo the previous task performed on the experiment.
6.‘Clear’ button is used to clear the canvas to start the experiment again.
7.‘Move’ button helps to relocate the components. Click on the button and then click on the component to be moved on the canvas.
8.The ‘On’ button the right top corner of the canvas is used to start and stop the simulation.
9.The ‘Click’ button on the CRO helps to show the waveforms of the experiment.
10.The output waveform will only be shown if the simulation is running.
11.The ‘Frequency response’ button is used to plot the frequency response of the experiment.
12.Enter the ‘Initial Frequency’, ‘Last Frequency’ and select the ‘Interval’ for the response.
</pre>


> ## Refrences :

<pre>

1.Op-Amps and Linear Integrated Circuits Technology, by Ramakant A. Gayakwad, Fourth Edition
2.Operational Amplifiers, by George Clayton and Steve Winder , Fifth Edition

</pre>

>  ## Theory :  
A simple differentiator circuit is obtained by interchanging the position of the resistor and capacitor in the basic integrator circuit. The ideal performance equation for the simple differentiator is readily derived from the usual ideal amplifier assumptions. Since the input signal is applied through a capacitor there is current flow to the amplifier summing point and a nonzero output voltage only when the input voltage changes.

Assume current 'I' is flowing through capacitor C. It is given as

Since input current to the op-amp is zero, same current 'I' flows through resistance R as shown. It is given by

Equating both the above equations of 'I' we get,

Thus, output voltage is nothing but time differentiation of the input signal and hence acting as differentiator. Here 'RC' is the time constant of the differentiator.
Now let us see what is the response of the differentiator to the different types of input signals.
1)Vin = Step signal
The step signal is defined as follows
Vin (t)=A for t>0
Let the product RC = 1.
The output voltage is given as

Practically the step input is taking some finite time to rise from 0 to magnitude A. Due to this small interval of time, the differentiator output is not zero; but appears in the form of spikes at t = 0. The input and output waveform is shown below.

2)Vin = Square Wave The square wave is nothing but combination of positive and negative step signals. As seen in first case, the output of step signal is a spike. For positive step signal, a negative spike is obtained because it is inverting differentiator. Thus, for a square wave input, the output obtained is a spike waveform as shown in figure below.

3)Vin = Sine Wave
Let Vin=Vm sinωt

Assuming the time constant RC = 1 and taking the differentiation, we get


Frequency Response of ideal integrator:
The voltage gain for the same is given as

The magnitude of gain A is |A|=|2πfRC|
Thus, the gain A is directly proportional to frequency f. At low frequency, the gain is also low. As frequency increases, gain also increases linearly at the rate of 20dB/decade. For dc input (f = 0) the gain is zero. Let, the frequency fa is defined as follows
fa=1/2πRC
Therefore the gain A is given as
|A|=|f/fa |
Thus When f < fa, the gain A is less than 1( i.e. negative) When f = fa, then the gain is 1 (i.e. 0dB)
Thus the frequency fa is nothing but the 0 dB (or unity gain) frequency for differentiator. When f > fa, the gain A is increases linearly at the rate of 20dB/decade. The frequency response of ideal differentiator is shown in figure below.

Disadvantages of ideal differentiator:
>
The reactance of the capacitor Xc is given as Xc=1/2πfC
As frequency increases, Xc reduces i.e. Capacitor draws more current from input source. Thus, at high frequency input source gets loaded by capacitor C. Therefore, noise signals at higher frequency gets amplified and appeared at output. So, to avoid the mis-amplification of the signal the ideal differentiator is modified and referred as practical differentiator or compensated differentiator.



